import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
mpl.style.use('seaborn')

diff_4 = {
    1: 10362121.5, 
    2: 5400008.2,
    3: 5142050.5,
    4: 6894518.6,
    8: 4855211.3
}

diff_5 = {
    1: 228438720.4,
    2: 94808345.6,
    3: 131101546.8,
    4: 93943397.8,
    8: 69378626.4 
}

diff_6 = {
    1: 3166994969,
    2: 1962058737,
    3: 1685313819,
    4: 2623015484,
    8: 1811619417
}

if __name__ == '__main__': 
    plt.figure()
    plt.subplot(1,3,1)
    plt.title("Difficulty 4")
    plt.xlabel("Threads")
    plt.ylabel("No. of Ticks")
    plt.bar(diff_4.keys(),diff_4.values())
    plt.subplot(1,3,2)
    plt.title("Difficulty 5")
    plt.xlabel("Threads")
    plt.ylabel("No. of Ticks")
    plt.bar(diff_5.keys(),diff_5.values())
    plt.subplot(1,3,3)
    plt.title("Difficulty 6")
    plt.xlabel("Threads")
    plt.ylabel("No. of Ticks")
    plt.bar(diff_6.keys(),diff_6.values())
    plt.show()